<?php

/**
 * @file
 * Contains CisionFeedsFetcher and CisionFeedsFetcherResult.
 */

define('CISION_FEED_DEFAULT_PAGE_SIZE', 50);
define('CISION_FEED_MAX_PAGE_SIZE', 100);
define('CISION_FEED_CURL_DEFAULT_TIMEOUT', 5);
define('CISION_FEED_VERSION', '1.x');

/**
 * CisionFetcher class.
 */
class CisionFeedsFetcher extends FeedsFetcher {

  /**
   * Fetches a feed.
   *
   * @param FeedsSource $source
   *   Source value as entered by user through sourceForm().
   *
   * @return CisionFeedsFetcherResult
   *   A CisionFetcherResult object.
   */
  public function fetch(FeedsSource $source) {
    return new CisionFeedsFetcherResult($source, $this);
  }

  /**
   * Returns the default configurations.
   *
   * @return array
   *   An array with default configurations.
   */
  public function configDefaults() {
    $options = $this->getTypes();

    return array(
      'cision_feed_types' => array_keys($options),
      'cision_feed_curl_timeout' => CISION_FEED_CURL_DEFAULT_TIMEOUT,
      'cision_feed_page_size' => CISION_FEED_DEFAULT_PAGE_SIZE,
      'cision_feed_page_index' => 1,
      'cision_feed_start_date' => '',
      'cision_feed_end_date' => '',
      'cision_feed_language' => 'en',
      'cision_feed_tags' => '',
    );
  }

  /**
   * Returns all different message types available in cision.
   *
   * @return array
   *   An array of all message types.
   */
  public function getTypes() {
    return array(
      'KMK' => t('Annual Financial statement'),
      'RDV' => t('Annual Report'),
      'PRM' => t('Company Announcement'),
      'RPT' => t('Interim Report'),
      'INB' => t('Invitation'),
      'NBR' => t('Newsletter'),
    );
  }

  /**
   * Returns an array of all enabled languages.
   *
   * @return array
   *   Returns the available languages.
   */
  public function getLanguages() {
    $options = array('' => t('Select language'));
    $languages = language_list();
    foreach ($languages as $language) {
      $options[$language->language] = $language->name;
    }
    return $options;
  }

  /**
   * Returns a configuration form for this fetcher.
   *
   * @param array $form_state
   *   FormAPI style form array.
   *
   * @return array
   *   FormAPI style form definition.
   */
  public function configForm(&$form_state) {
    $form['cision_feed_language'] = array(
      '#type' => 'select',
      '#title' => t('Language'),
      '#description' => t('Language code of the default language in the feed.'),
      '#options' => $this->getLanguages(),
      '#default_value' => $this->config['cision_feed_language'],
    );
    $form['cision_feed_types'] = array(
      '#type' => 'select',
      '#title' => t('Select type of messages'),
      '#options' => $this->getTypes(),
      '#multiple' => TRUE,
      '#default_value' => $this->config['cision_feed_types'],
      '#description' => t('The type of releases to collect from cision.'),
    );
    $form['cision_feed_page_size'] = array(
      '#type' => 'textfield',
      '#title' => t('Page size'),
      '#description' => t('The number of releases returned from cision. The default is 50 and the max is 100.'),
      '#default_value' => $this->config['cision_feed_page_size'],
    );
    $form['cision_feed_page_index'] = array(
      '#type' => 'textfield',
      '#title' => t('Page index'),
      '#description' => t('The index the release list starts on. The default is 1.'),
      '#default_value' => $this->config['cision_feed_page_index'],
    );
    $form['cision_feed_start_date'] = array(
      '#type' => 'textfield',
      '#title' => t('Start date'),
      '#description' => t('Start date in UTC format.'),
      '#default_value' => $this->config['cision_feed_start_date'],
    );
    $form['cision_feed_end_date'] = array(
      '#type' => 'textfield',
      '#title' => t('End date'),
      '#description' => t('End date in UTC format.'),
      '#default_value' => $this->config['cision_feed_end_date'],
    );
    $form['cision_feed_tags'] = array(
      '#type' => 'textfield',
      '#title' => t('Tags'),
      '#description' => t('Only include releases with specified tags. One or several tags can be provided separated with a comma.'),
      '#default_value' => $this->config['cision_feed_tags'],
    );
    return $form;
  }

  /**
   * Validate the configuration form.
   *
   * @param array $values
   *   An array that contains the values entered by the user through configForm.
   */
  public function configFormValidate(&$values) {
    if ($values['cision_feed_page_size'] > CISION_FEED_MAX_PAGE_SIZE ||
        $values['cision_feed_page_size'] < 1) {
      $values['cision_feed_page_size'] = CISION_FEED_DEFAULT_PAGE_SIZE;
    }
    if ($values['cision_feed_page_index'] < 0) {
      $values['cision_feed_page_index'] = 1;
    }
    if (!empty($values['cision_feed_start_date'])) {
      $date = DateTime::createFromFormat('Y-m-d', $values['cision_feed_start_date']);
      if (!$date || $date->format('Y-m-d') !== $values['cision_feed_start_date']) {
        $values['cision_feed_start_date'] = '';
        form_set_error('cision_feed_start_date', t('Invalid format. Use a format like 2017-01-01.'));
      }
    }
    if (!empty($values['cision_feed_end_date'])) {
      $date = DateTime::createFromFormat('Y-m-d', $values['cision_feed_end_date']);
      if (!$date || $date->format('Y-m-d') !== $values['cision_feed_end_date']) {
        $values['cision_feed_end_date'] = '';
        form_set_error('cision_feed_end_date', t('Invalid format. Use a format like 2017-01-01.'));
      }
    }
    if (!empty($values['cision_feed_start_date']) && !empty($values['cision_feed_end_date'])) {
      if ($values['cision_feed_end_date'] > $values['cision_feed_start_date']) {
        $values['cision_feed_end_date'] = '';
        $values['cision_feed_start_date'] = '';
        form_set_error('cision_feed_start_date', t('The start date cannot be greater than the end date.'));
        form_set_error('cision_feed_end_date', t('The end date cannot be lower than the start date.'));
      }
    }
  }

  /**
   * Source form.
   *
   * @param array $source_config
   *   The source configuration.
   *
   * @return array
   *   FormAPI style form definition.
   */
  public function sourceForm($source_config) {
    $form = array();
    $form['source'] = array(
      '#type' => 'textfield',
      '#title' => t('URL'),
      '#description' => t('Enter a feed URL.'),
      '#default_value' => empty($source_config['source']) ? '' : $source_config['source'],
    );
    return $form;
  }

}

/**
 * CisionFetcherResult class.
 */
class CisionFeedsFetcherResult extends FeedsFetcherResult {
  protected $config;
  protected $url;

  /**
   * Constructor.
   */
  public function __construct(FeedsSource $source, FeedsFetcher $fetcher) {
    $source_config = $source->getConfigFor($fetcher);
    $this->url = $source_config['source'];
    $this->config = $fetcher->getConfig();
  }

  /**
   * Finds a release based on its id.
   *
   * @param stdClass $data
   *   An object with all feeds.
   * @param string $release_id
   *   Id of the release to search for.
   *
   * @return stdClass
   *   The release with the supplied id.
   */
  private function getReleaseById(stdClass $data, $release_id) {
    if (is_object($data) && isset($data->Releases)) {
      foreach ($data->Releases as $release) {
        if ($release->Id == $release_id) {
          return $release;
        }
      }
    }
  }

  /**
   * Fetches a feed from the supplied URL.
   *
   * @param string $url
   *   The feed url to fetch.
   * @param array $args
   *   Options for the request.
   * @param int $http_code
   *   Optional argument used to transport the http response code.
   *
   * @return string
   *   The json encoded feed data or NULL.
   */
  protected function fetchUrl($url, array $args = array(), &$http_code = NULL) {
    $feed = NULL;
    $url = url($url, array('query' => $args));
    $response = drupal_http_request($url);
    $http_code = $response->code;
    switch ($response->code) {
      case 304:
        break;

      case 301:
      case 200:
      case 302:
      case 307:
        if (isset($response->data)) {
          $feed = json_decode($response->data);
        }
        break;

      default:
        break;
    }
    return $feed;
  }

  /**
   * Sets the tid for all LanguageVersions of an item.
   *
   * @param stdClass $feed_base
   *   The base feed.
   */
  private function handleTranslation(stdClass $feed_base) {
    foreach ($feed_base->Releases as $feed_item) {
      if (!isset($feed_item->Tid)) {
        $feed_item->Tid = $feed_item->Id;
      }
      if (count($feed_item->LanguageVersions)) {
        foreach ($feed_item->LanguageVersions as $version) {
          $translation = $this->getReleaseById($feed_base, $version->ReleaseId);
          if ($translation) {
            $translation->Tid = $feed_item->Id;
          }
        }
      }
    }
  }

  /**
   * Filters the feed releases.
   *
   * Removes any language versions from all releases not in
   * the selected language. This function also removes all
   * feeds that is not of the selected InformationType.
   *
   * @param stdClass $feed
   *   The base feed.
   */
  private function filterFeed(stdClass $feed) {
    foreach ($feed->Releases as $index => $release) {
      if (count($release->LanguageVersions)) {
        if ($release->LanguageCode != $this->config['cision_feed_language']) {
          $release->LanguageVersions = [];
        }
      }

      if (!in_array($release->InformationType, $this->config['cision_feed_types'])) {
        unset($feed->Releases[$index]);
      }
    }

    // Sort all releases based on selected language.
    usort($feed->Releases, function ($a, $b) {
      return ($a->LanguageCode != $this->config['cision_feed_language']);
    });
  }

  /**
   * Overrides FeedsFetcherResult::getRaw().
   *
   * @return string
   *   The raw content from the source as a string.
   */
  public function getRaw() {
    $query_args = array(
      'PageSize' => $this->config['cision_feed_page_size'],
      'PageIndex' => $this->config['cision_feed_page_index'],
      'StartDate' => $this->config['cision_feed_start_date'],
      'EndDate' => $this->config['cision_feed_end_date'],
      'DetailLevel' => 'detail',
      'Format' => 'json',
      'Tags' => $this->config['cision_feed_tags'],
    );

    $http_code = 0;
    $feed_base = $this->fetchUrl($this->url, $query_args, $http_code);
    if ($feed_base && $http_code == 200) {
      $this->filterFeed($feed_base);
      $this->handleTranslation($feed_base);
      $this->raw = json_encode($feed_base);
    }
    else {
      drupal_set_message(t('The request returned a http response code of @code', array('@code' => $http_code)), 'error');
      watchdog('Cision Feed', 'The request returned a http response code of @code', array('@code' => $http_code), WATCHDOG_DEBUG);
    }

    return $this->raw;
  }

}
