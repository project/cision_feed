INTRODUCTION
============

This module is providing a custom parser and a fetcher 
for the Feeds module, adding support to import content from cision. 
Cision Feed creates translations if a feed item has connected versions 
in another language.

REQUIREMENTS
============

- Feeds 7.x-1.x
  http://drupal.org/project/feeds
- Drupal 7.x
  http://drupal.org/project/drupal

INSTALLATION
============

- Install and enable feeds and cision_feed modules.

CONFIGURATION
=============

- Add a new importer at admin/structure/feeds. 
- Set the fetcher to Cision Fetcher.
- Set the parser to Cision Parser.
- Configure the settings for the fetcher.
- Use the node processor and choose which
bundle to use.
- Setup mappings under admin/structure/feeds/<importer>/mapping.
- Go to the import page and add your source for the feed.

MAINTAINERS
-----------

Current maintainers:
 * Krister Andersson (Cyclonecode) - https://www.drupal.org/user/1225156
