<?php

/**
 * @file
 * Contains CisionFeedsParser.
 */

/**
 * CisionFeedsParser class.
 */
class CisionFeedsParser extends FeedsParser {

  /**
   * Parses a raw string and populates FeedsParserResult object from it.
   *
   * @param FeedsSource $source
   *   Source information.
   * @param FeedsFetcherResult $fetcher_result
   *   FeedsFetcherResult returned by fetcher.
   *
   * @return FeedsParserResult
   *   FeedsFetcherResult returned by fetcher.
   */
  public function parse(FeedsSource $source, FeedsFetcherResult $fetcher_result) {
    $string = $fetcher_result->getRaw();
    $batch = new FeedsParserResult();

    // The parsed result should be an array of arrays of field name => value.
    $data = json_decode($string);
    if (is_object($data) && is_array($data->Releases)) {
      foreach ($data->Releases as $release) {
        $images = NULL;
        $files = NULL;
        $quotes = NULL;
        $externallinks = NULL;
        $embeddeditems = NULL;

        if (count($release->Images)) {
          $images = array();
          foreach ($release->Images as $img) {
            $images['uri'][] = check_url($img->DownloadUrl);
            $images['alt'][] = check_plain($img->Description);
            $images['title'][] = check_plain($img->Title);
          }
        }
        if (count($release->Files)) {
          $files = array();
          foreach ($release->Files as $file) {
            $files['uri'][] = check_url($file->Url);
            $files['description'][] = check_plain($file->Description);
          }
        }
        if (count($release->Quotes)) {
          $quotes = array();
          foreach ($release->Quotes as $quote) {
            $quotes['text'][] = check_plain($quote->Text);
            $quotes['author'][] = check_plain($quote->Author);
          }
        }
        if (count($release->ExternalLinks)) {
          $externallinks = array();
          foreach ($release->ExternalLinks as $link) {
            $externallinks['uri'][] = check_url($link->Url);
            $externallinks['title'][] = check_plain($link->Title);
          }
        }
        if (count($release->EmbeddedItems)) {
          $embeddeditems = array();
          foreach ($release->EmbeddedItems as $embeddeditem) {
            $embeddeditems[] = $embeddeditem->EmbedCode;
          }
        }

        $batch->items[] = array(
          'guid' => (int) $release->Id,
          'title' => check_plain($release->Title),
          'htmltitle' => check_markup($release->HtmlTitle),
          'header' => check_plain($release->Header),
          'htmlheader' => check_markup($release->HtmlHeader),
          'created' => strtotime($release->PublishDate),
          'changed' => strtotime($release->LastChangeDate),
          'intro' => check_plain($release->Intro),
          'htmlintro' => check_markup($release->HtmlIntro),
          'body' => check_plain($release->Body),
          'htmlbody' => check_markup($release->HtmlBody),
          'countrycode' => check_plain($release->CountryCode),
          'languagecode' => check_plain($release->LanguageCode),
          'isregulatory' => (int) $release->IsRegulatory,
          'socialmediapitch' => (isset($release->SocialMediaPitch) ? check_plain($release->SocialMediaPitch) : ''),
          'type' => check_plain($release->InformationType),
          'contact' => check_plain($release->Contact),
          'htmlcontact' => check_markup($release->HtmlContact),
          'image:uri' => $images['uri'],
          'image:alt' => $images['alt'],
          'image:title' => $images['title'],
          'file:uri' => $files['uri'],
          'file:description' => $files['description'],
          'cisionwireurl' => check_url($release->CisionWireUrl),
          'keywords' => $release->Keywords,
          'categories' => $release->Categories,
          'companyinformation' => check_plain($release->CompanyInformation),
          'htmlcompanyinformation' => check_markup($release->HtmlCompanyInformation),
          'quickfacts' => $release->QuickFacts,
          'logourl' => check_url($release->LogoUrl),
          'quote:text' => $quotes['text'],
          'quote:author' => $quotes['author'],
          'externallink:uri' => $externallinks['uri'],
          'externallink:title' => $externallinks['title'],
          'embeddeditems' => $embeddeditems,
          'tid' => $release->Tid,
        );
      }
    }

    // Populate the FeedsParserResult object with the parsed results.
    $batch->title = 'Cision feed batch';

    return $batch;
  }

  /**
   * Returns the mappings.
   *
   * @return array
   *   An array of mapping sources.
   */
  public function getMappingSources() {
    return array(
      'guid' => array(
        'name' => t('GUID'),
        'description' => t('The id of the release.'),
      ),
      'title' => array(
        'name' => t('Title'),
        'description' => t('The title of the release.'),
      ),
      'htmltitle' => array(
        'name' => t('HtmlTitle'),
        'description' => t('The title with the associated html-code that has been edited in the html editor in Cision Connect.'),
      ),
      'header' => array(
        'name' => t('Header'),
        'description' => t('The header of the release.'),
      ),
      'htmlheader' => array(
        'name' => t('HtmlHeader'),
        'description' => t('The header with the associated html-code that has been edited in the html editor in Cision Connect.'),
      ),
      'created' => array(
        'name' => t('PublishDate'),
        'description' => t('The date when the release was published. It uses the UTC time standard.'),
      ),
      'changed' => array(
        'name' => t('LastChangeDate'),
        'description' => t('The date when the release was last changed. It uses the UTC time standard.'),
      ),
      'intro' => array(
        'name' => t('Intro'),
        'description' => t('The preamble of the release.'),
      ),
      'htmlintro' => array(
        'name' => t('HtmlIntro'),
        'description' => t('The preamble with the associated html-code that has been edited in the html editor in Cision Connect.'),
      ),
      'body' => array(
        'name' => t('Body'),
        'description' => t('The body of the release in plain text.'),
      ),
      'htmlbody' => array(
        'name' => t('HtmlBody'),
        'description' => t('The body with the associated html-code that has been edited in the html editor in Cision Connect.'),
      ),
      'isregulatory' => array(
        'name' => t('Regulatory'),
        'description' => t('Boolian which states if the release contains regulatory information.'),
      ),
      'type' => array(
        'name' => t('InformationType'),
        'description' => t('The type code of the release.'),
      ),
      'languagecode' => array(
        'name' => t('LanguageCode'),
        'description' => t('The language code of the release.'),
      ),
      'countrycode' => array(
        'name' => t('CountryCode'),
        'description' => t('The country linked to this release.'),
      ),
      'contact' => array(
        'name' => t('Contact'),
        'description' => t('Contact information linked to the release.'),
      ),
      'htmlcontact' => array(
        'name' => t('HtmlContact'),
        'description' => t('The contact information with the associated html-code that has been edited in the html editor in Cision Connect.'),
      ),
      'image:uri' => array(
        'name' => t('Image'),
        'description' => t('Cision image.'),
      ),
      'image:alt' => array(
        'name' => t('Image Description'),
        'description' => t('Cision image description.'),
      ),
      'image:title' => array(
        'name' => t('Image Title'),
        'description' => t('Cision image title.'),
      ),
      'file:uri' => array(
        'name' => t('File'),
        'description' => t('Cision file.'),
      ),
      'file:description' => array(
        'name' => t('File description'),
        'description' => t('Cision file description.'),
      ),
      'keywords' => array(
        'name' => t('Keywords'),
        'description' => t('Keywords are Cisions version of tags.'),
      ),
      'categories' => array(
        'name' => t('Categories'),
        'description' => t('Cision categories.'),
      ),
      'cisionwireurl' => array(
        'name' => t('CisionWireUrl'),
        'description' => t('A URL leading to the release page on Cision News'),
      ),
      'socialmediapitch' => array(
        'name' => t('SocialMediaPitch'),
        'description' => t('The text that appears when sharing a release on social networks.'),
      ),
      'companyinformation' => array(
        'name' => t('CompanyInformation'),
        'description' => t('The company information linked to the release.'),
      ),
      'htmlcompanyinformation' => array(
        'name' => t('HtmlCompanyInformation'),
        'description' => t('The company information with the associated html-code that has been edited in the html editor in Cision Connect.'),
      ),
      'quickfacts' => array(
        'name' => t('QuickFacts'),
        'description' => t('Quick facts about the release or company.'),
      ),
      'logourl' => array(
        'name' => t('LogoUrl'),
        'description' => t('URL to the company logotype.'),
      ),
      'quote:text' => array(
        'name' => t('Quote'),
        'description' => t('Quote for the release.'),
      ),
      'quote:author' => array(
        'name' => t('Quote author'),
        'description' => t('Author of the quote.'),
      ),
      'externallink:uri' => array(
        'name' => t('ExternalLink'),
        'description' => t('External link related to the company or the release.'),
      ),
      'externallink:title' => array(
        'name' => t('ExternalLink Title'),
        'description' => t('Title for the external link.'),
      ),
      'embeddedttems' => array(
        'name' => t('EmbeddedItems'),
        'description' => t('Element containing information about items embedded to the release.'),
      ),
    );
  }

}
